// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// =============================================
/* if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
} */

// =============================================
// process.env SETUP
// =============================================
// const PORT = process.env.PORT || 5000; 
// const { NODE_ENV } = process.env;
const { NODE_ENV , PORT } = process.env;

// =============================================
// Establish the NODE_ENV toggle settings
// ==============================================
const IS_PRODUCTION_NODE_ENV = NODE_ENV === 'production'; // true
const IS_DEVELOPMENT_NODE_ENV = NODE_ENV === 'development'; // true

// =============================================
// BASE SETUP
// =============================================
const path = require("path");
const express = require("express");

// =============================================
// boilerplate
// =============================================
// const cors = require("cors");

// const bodyParser = require("body-parser");

const app = express();

// =============================================
// middleware - FINER DETAILS
// =============================================

// app.use( bodyParser.json() );
app.use( express.json() ); // for parsing application/json
app.use( express.urlencoded( { extended: true } ) ); // for parsing application/x-www-form-urlencoded

// app.use( cors() );

const posts = require("./routes/posts"); // const posts = require("./routes/api/posts");

app.use( "/api/posts" , posts );

// =============================================
// Serve static files from the Vue app
// =============================================
// UNCOMMENT THIS AND SET PORT IN .ENV TO 3000
if( (IS_PRODUCTION_NODE_ENV === true) || ( IS_DEVELOPMENT_NODE_ENV === true  ) ){
  app.use( express.static( path.join( __dirname, 'public' ) ) ); 
}

// =============================================
// The "catchall" handler that will handle all requests in order to return Vue's index.html file.
// =============================================
const catchAllGET = ( req, res, next ) => {
  res.sendFile( path.join( __dirname , 'public', 'index.html' ) );
};

// UNCOMMENT THIS AND SET PORT IN .ENV TO 3000
if( (IS_PRODUCTION_NODE_ENV === true) || ( IS_DEVELOPMENT_NODE_ENV === true  ) ){
  app.get( '*' , catchAllGET ); 
  // app.get( /.*/ , catchAllGET ); 
}

app.listen( PORT, () => { 
    
  // console.log( "PORT" , PORT );
  
  // console.log( `Backend is running.`);

  if( IS_PRODUCTION_NODE_ENV === true ){
    console.log( `Backend is running, baby.`);
  }
  if( IS_DEVELOPMENT_NODE_ENV === true ){
    console.log( `Production backend has started at http://127.0.0.1:${PORT} .`);
  }
  if(IS_PRODUCTION_NODE_ENV === false && IS_DEVELOPMENT_NODE_ENV === false ){ 
    console.log(`Development backend is now testing at port ${PORT} . Run and visit your frontend!`);
  }

} );