// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// =============================================
/* if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
} */

// =============================================
// process.env SETUP
// =============================================
// const { NODE_ENV } = process.env;
const { CONNECTION_URL } = process.env; 

const express = require("express");
const mongodb = require("mongodb");

const postRouter = express.Router();

// =============================================
// cursory db middleware 
// =============================================
const loadPostsCollection = async () => {
  
  const client = await mongodb.MongoClient.connect( CONNECTION_URL , {
    useNewUrlParser: true 
    , useUnifiedTopology: true 
    // , useFindAndModify: false 
    // , useCreateIndex: true
  } );

  return client.db( 'posts-atlas' ).collection( 'posts' );

};


// =============================================
// CRUD - sans read-one and update-one
// =============================================

// ADD post
const addpost = async( req, res, next ) => {
  const posts = await loadPostsCollection();
  await posts.insertOne( {
    text: req.body.text
    , createdAt: new Date()
  } );
  
  res.status(200).json( { message : "Post has been added." } );
  // res.status(201).send();
  // CULLS this... svc err:  SyntaxError: "JSON.parse: unexpected end of data at line 1 column 1 of the JSON data"
  
};

postRouter.post( "/" , addpost );


// GET posts
const getposts = async ( req, res, next ) => {
  // res.send("hello");
  const posts = await loadPostsCollection();
  res.send( await posts.find({}).toArray() );
};

postRouter.get( "/" , getposts );


// DELETE post - v1 modified
const deletepost = async ( req, res, next ) => {
  
  const posts = await loadPostsCollection();
  
  await posts.deleteOne( {
    _id: new mongodb.ObjectID( req.params.id )
  } );

  
  // res.status(200).send();
  res.status(200).json( { message : "Post has been deleted." } );

};


postRouter.get( "/:id" , deletepost ); // postRouter.delete( "/:id" , deletepost );


module.exports = postRouter;