db.posts.drop();

db.posts.insert( {
	text: "Hello, this is my first post."
	, createdAt : new Date()
} );

db.posts.insert( {
	text: "My second post."
	, createdAt : new Date()
} );

db.posts.insert( {
  text: "This is the third post."
  , createdAt : new Date()
} );

// new
db.posts.insert( {
  text: "Hi, Todd! I'm Tucker!!!"
  , createdAt : new Date()
} );

// fin