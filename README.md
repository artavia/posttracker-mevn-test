# Description
Prototype blogpost tracker-- MEVN stack exercise --for use with Heroku and Mongo Atlas.

## Background 
While I am presently more versed and confident when handling each **React** and **Angular**, I felt it necessary to begin focusing on **VueJS**. I have a codebase as it relates generally to most anything that circles the **VueJS** orbit. I had until now, however, found it difficult to put something together that is presentable.

## Attribution
This item was originally conceived by [Brad Traversy](https://github.com/bradtraversy/microposts_fullstack_vue) and can be imitated in digestible chunks or videos found at [youtube](https://www.youtube.com/watch?v=j55fHUJqtyw)&#45;&#45; there are **three videos** in all.

## Differences between the original version and my own version
In terms of the server folder structure, the routes are burrowed in **one** level instead of **two** levels. Also, the route that corresponds to **record deletion** uses the **GET** verb, in lieu of, the **DELETE** verb.

Then, in terms of the client&#45;side, I decided to ditch the use of **axios** in favor of the almost ubiquitous **fetch API**. 

Finally, since some experience with Heroku is to my benefit, it takes almost no effort to accomodate the use of a **.env** file which will be inevitably &quot;thrown to the fire like thatch&quot; once that the Heroku environment variables are established and in place.
