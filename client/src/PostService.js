// import axios from "axios";

class PostService {
  
  constructor(){ 
    // this.earl = `http://127.0.0.1:5000/api/posts`;
    this.earl = `/api/posts`;  
  }

  // Get Posts -- WITHOUT AXIOS or `static` operator - v2
  getPosts(){

    let postsRequest = new Request( this.earl );
    let getpostsOptions = {};

    return this.customfetch( postsRequest , getpostsOptions )
    .then( (data) => {
      // console.log( "getPosts... data" , data );
      return Promise.resolve( data );
    } )
    .then( (json) => {
      
      // console.log( 'json' , json );
      
      const phunckymap = ( post ) => {
        return ( {
          ...post
          , createdAt: new Date( post.createdAt )
        } );
      }; 

      let allposts = json.map( phunckymap );
      // console.log( "svc allposts: " , allposts ); 
      return allposts;

    } )
    .catch( err => {
      console.log( "err:" , err );
    } );
  } 

  // Create Post -- WITHOUT AXIOS or `static` operator - v2
  insertPost( text ){
    let newuserobj = { text };
    let insertRequest = new Request( this.earl );
    
    let insertOptions = {
      method: 'POST'
      , body: JSON.stringify(newuserobj)
    };

    return this.customfetch( insertRequest, insertOptions )
    .then( ( data ) => { 
      // console.log( "insertPost... data" , data );
      return Promise.resolve( data );
    } )
    .catch( err => console.log( "svc err: " , err ) );
  }
  
  // Delete Post -- WITHOUT AXIOS or `static` operator - v2
  deletePost( id ) {
    
    let deleteRequest = new Request( `${ this.earl }/${id}` );
    let deleteOptions = {};

    return this.customfetch( deleteRequest , deleteOptions )
    .then( (data) => {
      // console.log( "deletePost... data" , data );
      return Promise.resolve( data );
    } )
    //.then( (json) => { console.log( "svc json: " , json ); } )
    .catch( err => {
      console.log( "err:" , err );
    } );
  }

  customfetch( url, options ){
    
    let myRequest = new Request( url );

    let myHeaders = new Headers();
    myHeaders.append( 'Content-type' , 'application/json; charset=UTF-8' ); 
    myHeaders.append( "Accept" , "application/json" );
    // if( this.loggedIn() ){
    //   myHeaders.append( "Authorization" , `Bearer ${this.getToken() }` );
    // }

    let optionsobject = { headers: myHeaders, ...options };

    return fetch( myRequest, optionsobject )
    .then( this.checkStatus )
    .then( response => response.json() );
  }

  checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      return Promise.resolve(response); // return response;
    } 
    else {
      let error = new Error( response.statusText );
      error.response = response;
      return Promise.reject( error ); // throw error;
    }
  }

  /* 
  // Get Posts ~ WITH AXIOS - v1
  getPosts(){

    return axios.get( this.earl )
    .then( res => {
      // console.log( "svc res: " , res );
      return res.data;
    } )
    .then( ( json ) => {

      // console.log( "json: " , json ); 
      
      const phunckymap = ( post ) => {
        return ( {
          ...post
          , createdAt: new Date( post.createdAt )
        } );
      }; 

      let allposts = json.map( phunckymap );
      // console.log( "svc axios version allposts: " , allposts ); 
      // this.posts = allposts;
      return allposts;

    } )
    .catch( err => {
      console.log( "svc err: ", err );
      return err; 
      // return err.message;
    } );
  }

  // Create Post ~ WITH AXIOS - v1  
  insertPost( text ){
    axios.post( this.earl , { text : text } )
    .then( res => console.log( "svc res: " , res ) )
    .catch( err => console.log( "svc err: " , err ) );
  } 

  // Delete Post ~ WITH AXIOS - v1
  deletePost( id ){
    axios.delete( `${this.earl}/${id}` )
    .then( res => console.log( "svc res: " , res ) )
    .catch( err => console.log( "svc err: " , err ) );
  }  */

}

export { PostService };